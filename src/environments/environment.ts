// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDoA0LVZlAvqkbnFzb4qo4XtPgPnx2iuVo',
    authDomain: 'dkvotesystem.firebaseapp.com',
    databaseURL: 'https://dkvotesystem.firebaseio.com',
    projectId: 'dkvotesystem',
    storageBucket: 'dkvotesystem.appspot.com',
    messagingSenderId: '307561621830',
    appId: '1:307561621830:web:b344d25aeab2754fb39d47',
    measurementId: 'G-45QESWRKPX'
  },
  salt: "$2a$10$B44CJiOyfrTYU7Amcn2RdO"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
