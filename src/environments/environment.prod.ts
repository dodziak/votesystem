export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: 'AIzaSyDoA0LVZlAvqkbnFzb4qo4XtPgPnx2iuVo',
    authDomain: 'dkvotesystem.firebaseapp.com',
    databaseURL: 'https://dkvotesystem.firebaseio.com',
    projectId: 'dkvotesystem',
    storageBucket: 'dkvotesystem.appspot.com',
    messagingSenderId: '307561621830',
    appId: '1:307561621830:web:b344d25aeab2754fb39d47',
    measurementId: 'G-45QESWRKPX'
  },
  salt: "$2a$10$B44CJiOyfrTYU7Amcn2RdO"
};
