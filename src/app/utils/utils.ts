
export function converStringDateToDate(date: string | Date): Date {
    if (date instanceof Date) {
        return date;
    } else if (typeof (date) === 'string') {
        return new Date(date);
    } else {
        throw new Error('ZŁY FORMAT');
    }
}


export function convertDateToString(date: Date): string {
    console.log(date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate());
    let monthString = (date.getMonth() + 1).toString();

    if (monthString.length === 1) { monthString = "0" + monthString; }

    let dayString = date.getDate().toString();
    if (dayString.length === 1) { dayString = "0" + dayString; }

    return date.getFullYear() + '-' + monthString + '-' + dayString;
}

export function deleteNumbersFromString(text: string) {
    return text.replace(/[0-9]/g, '');
}