import { Injectable } from '@angular/core';
import { DbRoom } from '../pages/dashboard/rooms/create/create.component';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class ListRoomService {

  rooms: DbRoom[] = [];
  private _searchString = "";
  private _sortType: 'desc' | 'asc' = 'desc';

  constructor(private db: AngularFirestore) {
    this.reloadList();
  }

  setSortType(sortType) {
    this._sortType = sortType;
    this.reloadList();
  }

  setSearchString(searchString) {
    this._searchString = searchString;
    this.reloadList();
  }

  orderAllBy() {
    this.db.collection<DbRoom>('rooms', ref => ref.orderBy('createdOn', this._sortType))
      .snapshotChanges()
      .subscribe(roomList => {
        const rooms = [];

        for (let room of roomList) {

          const dbRoom = {
            ...room.payload.doc.data(),
            id: room.payload.doc.id
          }

          rooms.push(dbRoom);
        }

        this.rooms = rooms;
      });
  }

  searchRooms() {
    this.db.collection<DbRoom>('rooms', ref => ref.where('access.roomName', '==', this._searchString.toLowerCase())
      .orderBy('createdOn', this._sortType))
      .snapshotChanges()
      .subscribe(roomList => {
        const rooms = [];

        for (let room of roomList) {

          const dbRoom = {
            ...room.payload.doc.data(),
            id: room.payload.doc.id
          }

          rooms.push(dbRoom);
        }

        this.rooms = rooms;
        console.log(this.rooms)
      });
  }

  reloadList() {
    if (this._searchString) {
      this.searchRooms();
    } else {
      this.orderAllBy();
    }
  }
}
