import { Injectable } from '@angular/core';
import { DbRoom } from '../pages/dashboard/rooms/create/create.component';
import { AngularFirestore } from 'angularfire2/firestore';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  rooms: DbRoom[] = [];
  private _searchString = "";
  private _sortType: 'desc' | 'asc' = 'desc';
  userId;

  constructor(private db: AngularFirestore, private _authService: AuthService) {
    this._authService.authState$.subscribe(user => {
      if (user) { this.userId = user.uid; }

      this.reloadList();
    });
   }

   setSortType(sortType) {
    this._sortType = sortType;
    this.reloadList();
  }

  setSearchString(searchString) {
    this._searchString = searchString;
    this.reloadList();
  }

   reloadList() {
    if (this._searchString) {
      this.searchRooms();
    } else {
      this.orderAllBy();
    }
  }

  searchRooms() {
    this.db.collection<DbRoom>('rooms', ref => ref.where('access.roomName', '==', this._searchString.toLowerCase())
      .where('users', 'array-contains', this.userId)
      .orderBy('createdOn', this._sortType))
      .snapshotChanges()
      .subscribe(roomList => {
        const rooms = [];

        for (let room of roomList) {

          const dbRoom = {
            ...room.payload.doc.data(),
            id: room.payload.doc.id
          }

          rooms.push(dbRoom);
        }

        this.rooms = rooms;
      });
  }

  orderAllBy() {
    this.db.collection<DbRoom>('rooms', ref => ref.where('users', 'array-contains', this.userId)
      .orderBy('createdOn', this._sortType))
      .snapshotChanges()
      .subscribe(roomList => {
        const rooms = [];

        for (let room of roomList) {

          const dbRoom = {
            ...room.payload.doc.data(),
            id: room.payload.doc.id
          }

          rooms.push(dbRoom);
        }

        this.rooms = rooms;
      });
  }

}
