import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class CreateRoomService {

  constructor(private _db: AngularFirestore) { }

  addRoom(requestModel: RoomRequestModel) {
    return this._db.collection('rooms').add(requestModel)
    .then(result => console.log("Dodano nowy pokój !"))
    .catch(err => console.error(err));
  }
}


export interface RoomRequestModel {
  access: {
    roomName: string;
    passwordHash: string;
  };
  createdBy: string;
  createdOn: Date;
  endDate: Date;
  users: string[];
  voteInfo: {
    question: string;
    yes: string[];
    no: string[];
    hold: string[];
  }
}