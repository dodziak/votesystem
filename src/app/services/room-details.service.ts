import { AuthService } from 'src/app/services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subject } from 'rxjs';
import { DbRoom } from './../pages/dashboard/rooms/create/create.component';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ConditionalExpr } from '@angular/compiler';
import { firestore } from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class RoomDetailsService {

  constructor(private _db: AngularFirestore) { }

  findRoomByPass(password): Observable<firestore.QuerySnapshot> {
    return this._db.collection<DbRoom>('rooms', ref => ref
    .where('access.passwordHash', '==', password)).get();
  }

  findRoomById(id) {
    return this._db.doc<DbRoom>(`rooms/${id}`);
  }
  // findRoomById(id).ref ... ustawienie daty
}
