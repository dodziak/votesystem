import { AuthService } from 'src/app/services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable, Subject } from 'rxjs';
import { DbRoom } from './../pages/dashboard/rooms/create/create.component';
import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ConditionalExpr } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class UserRoomsService {

  userId;
  userRoomList: DbRoom[] = [];
  private _sortType: 'desc' | 'asc' = 'desc';
  private _searchString = "";


  constructor(private db: AngularFirestore, private _authService: AuthService) {
    this._authService.authState$.subscribe(user => {
      if (user) { this.userId = user.uid; }

      this.reloadList();
    });
  }

  setSearchString(searchString) {
    this._searchString = searchString;
    this.reloadList();
  }



  setSortType(sortType) {
    this._sortType = sortType;
    this.reloadList();
  }

  orderBy() {
    return this.db.collection<DbRoom>('rooms', ref => ref
      .where('createdBy', '==', this.userId)
      .orderBy('createdOn', this._sortType))
      .snapshotChanges()
      .subscribe(roomList => {
        const rooms = [];

        for(let room of roomList) {

          const dbRoom = {
            ...room.payload.doc.data(),
            id: room.payload.doc.id
          }

          rooms.push(dbRoom);
        }

        this.userRoomList = rooms;
      });
  }

  searchRooms() {
    this.db.collection<DbRoom>('rooms', ref => ref
      .where('access.roomName', '==', this._searchString.toLowerCase())
      .where('createdBy', '==', this.userId)
      .orderBy('createdOn', this._sortType))
      .snapshotChanges()
      .subscribe(roomList => {
        const rooms = [];

        for (let room of roomList) {

          const dbRoom = {
            ...room.payload.doc.data(),
            id: room.payload.doc.id
          }

          rooms.push(dbRoom);
        }

        this.userRoomList = rooms;

      });
  }

  reloadList() {
    if (this._searchString) {
      this.searchRooms();
    } else {
      this.orderBy();
    }
  }
}