import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthComponent } from './pages/auth/auth.component';
import { LoginComponent } from './pages/auth/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { CreateComponent } from './pages/dashboard/rooms/create/create.component';
import { DetailsComponent } from './pages/dashboard/rooms/details/details.component';
import { ListComponent } from './pages/dashboard/rooms/list/list.component';
import { ResultsComponent } from './pages/dashboard/rooms/results/results.component';


const routes: Routes = [
  { path: '', component: DashboardComponent, children: [
    { path: 'create', component: CreateComponent },
    { path: 'details/:id', component: DetailsComponent },
    { path: 'list', component: ListComponent },
    { path: 'results', component: ResultsComponent }
  ] },
  {
    path: 'auth', component: AuthComponent, children: [
      { path: 'login', component: LoginComponent }
    ]
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
