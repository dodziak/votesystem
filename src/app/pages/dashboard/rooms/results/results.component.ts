import { AuthService } from './../../../../services/auth.service';
import { ResultsService } from './../../../../services/results.service';
import { Component, OnInit } from '@angular/core';
import { DbRoom } from '../create/create.component';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  searchResultRooms: string = "";
  searchResultRoomsEvent: Subject<string> = new Subject<string>();

  sortType: 'desc' | 'asc';

  get roomsList() {
    return this._resService.rooms;
  }

  get user() {
    return this.authService.user;
  }

  constructor(private _resService: ResultsService, private authService: AuthService) {

   }

  ngOnInit() {

    this.searchResultRoomsEvent.pipe(
      debounceTime(300),
      distinctUntilChanged())
      .subscribe(model => {
        this.searchResultRooms = model;
        this._resService.setSearchString(this.searchResultRooms);
      });
  }

  userVoted(room: DbRoom) {
    const userLoged = this.isUserLogged(room);
    if (!userLoged) { return false; }

    const votedHold = room.voteInfo.hold.includes(this.authService.user.uid);
    const votedYes = room.voteInfo.yes.includes(this.authService.user.uid);
    const votedNo = room.voteInfo.no.includes(this.authService.user.uid);

    return votedHold || votedYes || votedNo;
  }

  isUserLogged(room: DbRoom) {
    const userLoged = room.users.includes(this.authService.user.uid);
    return userLoged;
  }

  userAnswered(room: DbRoom): string {
    const userVoted = this.userVoted(room);
    if (userVoted) {
      const votedHold = room.voteInfo.hold.includes(this.authService.user.uid);
      if (votedHold) { return 'Wstrzymano się'; }

      const votedYes = room.voteInfo.yes.includes(this.authService.user.uid);
      if (votedYes) { return 'Zagłosowano na tak'; }

      const votedNo = room.voteInfo.no.includes(this.authService.user.uid);
      if (votedNo) { return 'Zagłosowano na nie'; }
    }
    return '';
  }

  getAnswerColor(answer: string): string {
    switch (answer) {
      case 'Zagłosowano na tak':
        return 'badge-success';
      case 'Zagłosowano na nie':
        return 'badge-danger';
      case 'Wstrzymano się':
        return 'badge-warning';
    }
  }

  isVoteOver(room: DbRoom) {
    const endDate = room.endDate.toDate();
    if (endDate <= new Date()) {
      return true;
    }
  }

  searchStringChanged(text: string) {
    this.searchResultRoomsEvent.next(text);
  }

  orderBy(sortType) {
    this._resService.setSortType(sortType);
  }

  countYes(room: DbRoom) {
    const amount = room.voteInfo.yes.length;
    return amount;
  }

  countHold(room: DbRoom) {
    const amount = room.voteInfo.hold.length;
    return amount
  }

  countNo(room: DbRoom) {
    const amount = room.voteInfo.no.length;
    return amount;
  }

  countUsers(room: DbRoom) {
    const amount = room.users.length;
    return amount;
  }

}
