import { FirebaseDatabase } from '@angular/fire';
import { AuthService } from 'src/app/services/auth.service';
import { DbRoom } from './../create/create.component';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from 'angularfire2/firestore';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { RoomDetailsService } from 'src/app/services/room-details.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  private roomId: string = null;
  docRef: firebase.firestore.DocumentReference = null;
  public roomDetails = null;

  get yesVoteAmount() {
    return this.roomDetails.voteInfo.yes.length;
  }

  get noVoteAmount() {
    return this.roomDetails.voteInfo.no.length;
  }

  get holdVoteAmount() {
    return this.roomDetails.voteInfo.hold.length;
  }

  get usersAmount() {
    return this.roomDetails.users.length;
  }

  get isVotingOver() {
    return this.roomDetails.endDate.toDate() <= new Date();
  }


  constructor(private _db: AngularFirestore, private route: ActivatedRoute, private _rDetails: RoomDetailsService,
              private router: Router, private _authService: AuthService) {
    this.roomId = route.snapshot.paramMap.get('id');


  }

  ngOnInit() {
    const room = this._rDetails.findRoomById(this.roomId);
    this.docRef = room.ref;
    room.valueChanges().subscribe((x) => {
      this.roomDetails = x;
    });
  }

  backToList() {
    this.router.navigate(['/list']);
  }

  exitRoom() {
    const userVoted = this.userVoted();
    if (!userVoted) {
      const docRef = this._db.doc(`rooms/${this.roomId}`);
      docRef.set({
        users: firebase.firestore.FieldValue.arrayRemove(this._authService.user.uid)
      });
      this.router.navigate(['/list']);
    } else {
      alert('Nie możesz całkowicie wyjść z pokoju po oddaniu głosu, użyj  przycisku "Wróć do listy"');
    }

  }

  voteYes() {
    const userVoted = this.userVoted();
    if (!userVoted) {
      const docRef = this._db.doc(`rooms/${this.roomId}`);
      docRef.ref.set(
        { voteInfo: { yes: firebase.firestore.FieldValue.arrayUnion(this._authService.user.uid) } },
        { merge: true }
      );
    } else {
      alert('Nie można głosować dwa razy !');
    }
  }

  voteHold() {
    const userVoted = this.userVoted();
    if (!userVoted) {
      const docRef = this._db.doc(`rooms/${this.roomId}`);
      docRef.ref.set(
        { voteInfo: { hold: firebase.firestore.FieldValue.arrayUnion(this._authService.user.uid) } },
        { merge: true }
      );
    } else {
      alert('Nie można głosować dwa razy !');
    }
  }

  voteNo() {
    const userVoted = this.userVoted();
    if (!userVoted) {
      const docRef = this._db.doc(`rooms/${this.roomId}`);
      docRef.ref.set(
        { voteInfo: { no: firebase.firestore.FieldValue.arrayUnion(this._authService.user.uid) } },
        { merge: true }
      );
    } else {
      alert('Nie można głosować dwa razy !');
    }
  }

  userVoted() {
    const votedHold = this.roomDetails.voteInfo.hold.includes(this._authService.user.uid);
    const votedYes = this.roomDetails.voteInfo.yes.includes(this._authService.user.uid);
    const votedNo = this.roomDetails.voteInfo.no.includes(this._authService.user.uid);

    return votedHold || votedYes || votedNo;
  }

}