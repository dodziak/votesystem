import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { AuthService } from 'src/app/services/auth.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { converStringDateToDate } from 'src/app/utils/utils';
import { CreateRoomService, RoomRequestModel } from 'src/app/services/create-room.service';
import * as bcrypt from 'bcryptjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  createForm: FormGroup;
  isSaving = false;

  get roomName() {
    return this.createForm.get('roomName');
  }

  get password() {
    return this.createForm.get('password');
  }

  get question() {
    return this.createForm.get('question');
  }

  get endDate() {
    return this.createForm.get('endDate');
  }

  constructor(private _authService: AuthService, private _cRoom: CreateRoomService,) {
    this.buildForm();
  }

  ngOnInit() {

  }

  buildForm() {
    this.createForm = new FormGroup({
      roomName: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(4)]),
      question: new FormControl(null, Validators.required),
      endDate: new FormControl(null, Validators.required)
    });

  }


  saveQuestion() {

    if(!this.createForm.valid) {
      alert("BŁĄD WALIDACJI");
    }

    const dateObject = this.createForm.get('endDate').value;

    this.isSaving = true;

    const hashedPassword = bcrypt.hashSync(this.createForm.get('password').value, environment.salt);

    const questionModel: RoomRequestModel = {
      access: {
        roomName: this.createForm.get('roomName').value,
        passwordHash: hashedPassword
      },

      voteInfo: {
        question: this.createForm.get('question').value,
        no: [],
        hold: [],
        yes: [],
      },

      createdBy: this._authService.user.uid,
      createdOn: new Date(),
      users: [],
      endDate: new Date(dateObject.year, dateObject.month-1, dateObject.day),
    };

    this._cRoom.addRoom(questionModel).finally(() => {
      this.isSaving = false;
      this.buildForm();
    });
    alert('Pokój dodany pomyślnie')
  }
}



export interface DbRoom {
  id: string;
  access: Access;
  createdBy: string;
  createdOn: firebase.firestore.Timestamp;
  updatedOn: firebase.firestore.Timestamp;
  endDate: firebase.firestore.Timestamp;
  users: string[];
  voteInfo: VoteInfo;
}

export interface Access {
  roomName: string;
  passwordHash: string;
}

export interface VoteInfo {
  question: string;
  yes: string[];
  no: string[];
  hold: string[];
}

