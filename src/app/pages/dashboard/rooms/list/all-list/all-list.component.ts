import { Router } from '@angular/router';
import { DbRoom } from './../../create/create.component';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { ListRoomService } from 'src/app/services/list-room.service';
import { AuthService } from 'src/app/services/auth.service';
import { deleteNumbersFromString } from 'src/app/utils/utils';
import * as bcrypt from 'bcryptjs';
import { environment } from 'src/environments/environment.prod';
import { RoomDetailsService } from 'src/app/services/room-details.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-all-list',
  templateUrl: './all-list.component.html',
  styleUrls: ['./all-list.component.scss']
})
export class AllListComponent implements OnInit {

  passwordValue: string;
  errorText = null;

  get roomsList() {
    return this._liRoomService.rooms;
  }

  constructor(private db: AngularFirestore, private _liRoomService: ListRoomService,
              private authService: AuthService, private router: Router, private _rDetails: RoomDetailsService) { }

  ngOnInit() {
  }

  normalizeString = (text) => deleteNumbersFromString(text);

  isUserLogged(room: DbRoom) {
    const userLoged = room.users.includes(this.authService.user.uid);
    return userLoged;
  }


  passwordCheck(room: DbRoom) {
    const passHashed = bcrypt.hashSync(this.passwordValue, environment.salt);

    this._rDetails.findRoomByPass(passHashed).subscribe(x => { // wykluczenie wielu pokojów o tym samym haśle
      const properRoom = x.docs.find(z => z.id === room.id);

      if (!properRoom) { return alert('Złe hasło !'); } // tutaj trzeba wyświetlic stringa że hasło złe

      properRoom.ref.set(
        { users: firebase.firestore.FieldValue.arrayUnion(this.authService.user.uid) },
        { merge: true }
      ).then(() => {
          this.router.navigate(['details/', room.id]);
       })
      .catch(() => { alert('Błąd przy przekierowaniu !') });

    });
  }

  // sprawdzenie czy użytkownik zagłosował
  userVoted(room: DbRoom) {
    const userLoged = this.isUserLogged(room);
    if (!userLoged) { return false; }

    const votedHold = room.voteInfo.hold.includes(this.authService.user.uid);
    const votedYes = room.voteInfo.yes.includes(this.authService.user.uid);
    const votedNo = room.voteInfo.no.includes(this.authService.user.uid);

    return votedHold || votedYes || votedNo;
  }

  // sprawdzenie odpowiedzi użytkownika
  userAnswered(room: DbRoom): string {
    const userVoted = this.userVoted(room);
    if (userVoted) {
      const votedHold = room.voteInfo.hold.includes(this.authService.user.uid);
      if (votedHold) { return 'Wstrzymano się'; }

      const votedYes = room.voteInfo.yes.includes(this.authService.user.uid);
      if (votedYes) { return 'Głos na tak'; }

      const votedNo = room.voteInfo.no.includes(this.authService.user.uid);
      if (votedNo) { return 'Głos nie'; }
    }
    return '';
  }

  getAnswerColor(answer: string): string {
    switch (answer) {
      case 'Głos na tak':
        return 'badge-success';
      case 'Głos na nie':
        return 'badge-danger';
      case 'Wstrzymano się':
        return 'badge-warning';
    }
  }

  navigateToDetails(room) {
    this.router.navigate(['details/', room.id]);
  }

  isVoteOver(room: DbRoom) {
    const endDate = room.endDate.toDate();
    if (endDate <= new Date()) {
      return true;
    }
  }

}
