import { RoomDetailsService } from 'src/app/services/room-details.service';
import { Access, DbRoom } from './../../create/create.component';
import { Component, OnInit } from '@angular/core';
import { UserRoomsService } from 'src/app/services/user-rooms.service';
import { AuthService } from 'src/app/services/auth.service';
import { CreateRoomService } from 'src/app/services/create-room.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  get userRoomList() {
    return this._userRoomService.userRoomList;
  }


  constructor(private _userRoomService: UserRoomsService, private authService: AuthService, private _rDetails: RoomDetailsService) { }

  ngOnInit() {
  }

  isUserLogged(room: DbRoom) {
    const userLoged = room.users.includes(this.authService.user.uid);
    return userLoged;
  }

  userVoted(room: DbRoom) {
    const userLoged = this.isUserLogged(room);
    if (!userLoged) { return false; }

    const votedHold = room.voteInfo.hold.includes(this.authService.user.uid);
    const votedYes = room.voteInfo.yes.includes(this.authService.user.uid);
    const votedNo = room.voteInfo.no.includes(this.authService.user.uid);

    return votedHold || votedYes || votedNo;
  }

  userAnswered(room: DbRoom): string {
    const userVoted = this.userVoted(room);
    if (userVoted) {
      const votedHold = room.voteInfo.hold.includes(this.authService.user.uid);
      if (votedHold) { return "Wstrzymano się"; }

      const votedYes = room.voteInfo.yes.includes(this.authService.user.uid);
      if (votedYes) { return "Głos na tak"; }

      const votedNo = room.voteInfo.no.includes(this.authService.user.uid);
      if (votedNo) { return "Głos na nie"; }
    }
    return "";
  }

  getAnswerColor(answer: string): string {
    switch (answer) {
      case "Głos na tak":
        return "badge-success";
      case "Głos na nie":
        return "badge-danger";
      case "Wstrzymano się":
        return "badge-warning";
    }
  }

  endVoting(room: DbRoom) {
    this._rDetails.findRoomById(room.id).ref.update({
      endDate: firebase.firestore.Timestamp.fromDate(new Date())
    });
  }

  checkDate(room: DbRoom) {
    const endDate = room.endDate.toDate();
    if (endDate <= new Date()) {
      return true;
    }
  }

  deleteRoom(room: DbRoom) {
    this._rDetails.findRoomById(room.id).ref.delete();
  }

}
