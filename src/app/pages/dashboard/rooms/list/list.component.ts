import { UserRoomsService } from 'src/app/services/user-rooms.service';
import { Component, OnInit } from '@angular/core';
import { FirebaseDatabase } from '@angular/fire';
import { AngularFirestore } from 'angularfire2/firestore';
import { DbRoom } from '../create/create.component';
import { ListRoomService } from 'src/app/services/list-room.service';
import { AuthService } from 'src/app/services/auth.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  passwordValue: string;

  searchAllRooms: string = "";
  searchAllRoomsEvent: Subject<string> = new Subject<string>();

  searchRooms: string = "";
  searchRoomsEvent: Subject<string> = new Subject<string>();

  sortType: 'desc' | 'asc';

  get user() {
    return this._authService.user;
  }


  searchStringAllChanged(text: string) {
    this.searchAllRoomsEvent.next(text);
  }

  searchStringChanged(text: string) {
    this.searchRoomsEvent.next(text);
  }

  orderBy(sortType) {
    this._userRoomService.setSortType(sortType);
  }

  orderAllBy(sortType) {
    this._liRoomService.setSortType(sortType);
  }

  constructor(private _authService: AuthService, private _userRoomService: UserRoomsService, private _liRoomService: ListRoomService) { }

  ngOnInit() {
    this.searchAllRoomsEvent.pipe(
      debounceTime(300),
      distinctUntilChanged())
      .subscribe(model => {
        this.searchAllRooms = model;
        this._liRoomService.setSearchString(this.searchAllRooms);
      });

    this.searchRoomsEvent.pipe(
      debounceTime(300),
      distinctUntilChanged())
      .subscribe(model => {
        this.searchRooms = model;
        this._userRoomService.setSearchString(this.searchRooms);
      });
  }
}
