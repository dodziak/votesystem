import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  login() {
    this.authService.signinWithGoogle()
    .then((res) =>  console.log("UDAŁO SIĘ ZALOGOWAĆ"))
    .catch(err => { console.error("Nie udało się zalogować") });
   // .finally(() => this.isLoading = false);
  }
}
