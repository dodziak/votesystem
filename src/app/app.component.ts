import { Component, OnDestroy } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnDestroy {

  authGuardSubscriber: Subscription = null;

  constructor(private authService: AuthService, private router: Router) {
    this.authGuardSubscriber = this.authService.authState$.subscribe(user => {
      const isUserExists = !!user;
      this.resolveRoute(isUserExists);
    });
  }

  ngOnDestroy() {
    this.authGuardSubscriber.unsubscribe();
  }

  redirectToUrl(url: string) {
    console.log(`Przekierowanie do "${url}" na podstawie serwisu auth`);
    this.router.navigateByUrl(url);
  }

  resolveRoute(isUserLogged: boolean) {
    if (isUserLogged && this.router.url.includes('auth')) {
      this.redirectToUrl('/list')
    } else if (isUserLogged) {
      return;
    } else {
      this.redirectToUrl('auth/login');
    }
  }
}